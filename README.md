# Zoom Wow

_Created zoomie animated gifs to wow your frenemies!_

👉  Peep it here: **[zoom-wow.rewdy.lol](http://zoom-wow.rewdy.lol)**

## It is what it is 🤷‍♂️

This is a in-browser animated gif creator. It utilizes HTML5 canvas to create frames and it uses [GIFEncoder](https://github.com/eugeneware/gifencoder) to take the frames and create an animated gif.

Simple static site is setup in AWS with terraform and the rest is pretty basic.

Have fun!

👋
