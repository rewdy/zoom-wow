import React from "react";

import "./Header.scss";

export const Header: React.FC = () => {
  return (
    <header className="header-page">
      <div className="site-title h1">
        <span className="carrot">Zoom Wow!</span>
      </div>
      <div className="site-tagline">Make it biggerer</div>
    </header>
  );
};
