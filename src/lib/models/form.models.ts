export type FormModel = {
  text: string;
  size: "sm" | "md" | "lg";
  color: string;
  background: string;
};
